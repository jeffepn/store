@extends('template')
@section('conteudo')
<div class="jumbotron">
    <h1 class="display-4">Seminário Computação - Outubro 2019</h1>
    <p class="lead">
        Qualquer dúvida estarei a disposição.
    </p>
    <img src="{{url('images/contact.png')}}" alt="Contatos JP Desenvolvimento Web">
    <hr class="my-4">
    <a class="btn btn-outline-primary btn-lg" href="https://jpdesenvolvimentoweb.com.br/" role="button"> Saber mais</a>
</div>
@endsection
