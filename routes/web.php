<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('cadastrar-produto', 'ProdutoController@cadastrarProduto');
Route::post('registrar-produto', 'ProdutoController@storeProduto');

Route::get('listar-produtos', 'ProdutoController@list');

Route::get('editar-produto/{slug}', 'ProdutoController@editarProduto');
Route::post('atualizar-produto', 'ProdutoController@updateProduto');

Route::post('excluir-produto', 'ProdutoController@deleteProduto');

Route::get('listar-tipos', function () {
    return view('listar-tipos', ['tipos' => App\Models\Tipo::all()]);
});
