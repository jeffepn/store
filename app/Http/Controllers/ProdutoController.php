<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produto;
use App\Models\Tipo;

class ProdutoController extends Controller
{

    public function cadastrarProduto()
    {
        return view('cadastrar-produto', ['tipos' => Tipo::all()]);
    }

    public function storeProduto(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required|max:30',
            'preco' => 'required',
            'tipo_id' => 'required',
            'codigo_barra' => 'required|max:20',
            'descricao' => 'required'

        ], [
            'nome.required' => 'O produto precisa de um nome.',
            'nome.max' => 'Limite o campo a no máximo :max caracteres.',
            'preco.required' => 'Qual o preço do produto?',
            'tipo_id.required' => 'Escolha um tipo para o produto.',
            'descricao.required' => 'Escreva uma breve descrição do produto.',
            'codigo_barra.required' => 'O produto precisa de um código de barra.',
            'codigo_barra.max' => 'Limite o campo a no máximo :max caracteres.'
        ]);
        $data = $request->all();
        $data['slug'] = time() . rand(1111, 9999);
        if (Produto::create($data)) {
            return redirect()->back()->with('success', 'Produto cadastrado com sucesso.');
        }
        return redirect()->back()->with('error', 'Algo deu errado, tente novamente.');
    }
    public function list()
    {
        return view('listar-produtos', ['produtos' => Produto::all()]);
    }

    public function editarProduto($slug)
    {
        $produto = Produto::where('slug', $slug)
            ->first();
        if (!$produto) {
            return redirect()->to('listar-produtos')->with('error', 'Produto não encontrado em nossa base de dados.');
        }
        return view('editar-produto', ['produto' => $produto]);
    }

    public function updateProduto(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required|max:30',
            'preco' => 'required|regex:/^-?[0-9]+(?:\.[0-9]{1,2})$/|max:9',
            'tipo' => 'required',
            'codigo_barra' => 'required|max:20',
            'descricao' => 'required'

        ], [
            'nome.required' => 'O produto precisa de um nome.',
            'nome.max' => 'Limite o campo a no máximo :max caracteres.',
            'preco.required' => 'Qual o preço do produto?',
            'preco.max' => 'O preço máximo é 999999.99 .',
            'preco.regex' => 'Formato válido (99.99).',
            'tipo.required' => 'Escolha um tipo para o produto.',
            'descricao.required' => 'Escreva uma breve descrição do produto.',
            'codigo_barra.required' => 'O produto precisa de um código de barra.',
            'codigo_barra.max' => 'Limite o campo a no máximo :max caracteres.'
        ]);
        $data = $request->all();
        $produto = Produto::where('slug', $request->slug)->first();
        if (!$produto) {
            return redirect()->back()->with('error', 'Produto não encontrado em nossa base de dados.');
        }
        if ($produto->update($data)) {
            return redirect()->back()->with('success', 'Produto atualizado com sucesso.');
        }
        return redirect()->back()->with('error', 'Algo deu errado, tente novamente.');
    }

    public function deleteProduto(Request $request)
    {
        if (Produto::where('slug', $request->slug)->delete()) {
            return redirect()->back()->with('success', 'Produto excluído com sucesso.');
        }
        return redirect()->back()->with('error', 'Algo deu errado, tente novamente.');
    }
}
